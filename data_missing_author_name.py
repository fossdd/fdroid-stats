import yaml, glob

for f in glob.glob("metadata/*.yml"):
    with open(f) as fp:
        data = yaml.safe_load(fp)
    if "AuthorName" not in data:
        print(f[9:][:-4])
