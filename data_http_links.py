import yaml, glob

for f in glob.glob("metadata/*.yml"):
    with open(f) as fp:
        data = yaml.safe_load(fp)
    for item in data:
        if type(data[item]) == str:
            if data[item].startswith("http://"):
                print(f[9:][:-4])
